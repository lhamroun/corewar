Le corewar est un jeu de programmation dans lequel des champions s'affrontent
dans une machine virtuelle. Les champions sont des programmes écrit en redcode.
Le role d'un champion est de déclarer qu'il est en vie. Le dernier champion en
vie est gagnant. L'arène doit être initialisée selon les paramètres :
	- chaque champion possède un processus.
	- les processus sont équitablement espacé dans l'arène virtuelle.
	- un champion ne peut pas excéder une certaine taille.
	- les processus on 16 registres chacun.
Le détail du fonctionnement du jeu est dans le pdf.

Les processus sont représentés par des pointeurs sur la memoire. Ils vont lire
les instructions qu'ils pointent et doivent attendre un certain nombre de cycle
d'horloge selon la commande. Un processus meurt lorsqu'il n'a pas donné signe
de vie durant CYCLE_TO_DIE cycle(s). Les 16 commandes de corewar sont :
	- live
	- load
	- st
	- add
	- sub
	- or
	- xor
	- and
	- ldi
	- zjmp
	- sti
	- fork
	- lld
	- lldi
	- lfork
	- aff
Le détail de chacune des commandes est dans le pdf.

Ce projet est composé de 2 programmes :

	- asm : ce programme est un assembleur permettant de traduire du redcode en
			bytecode. Ce programme permettra de rendre le code des champions
			interprétable par la machine virtuelle. Tous les détails sur
			l'assembleur sont décrit dans le pdf.

	- corewar : La machine virtuelle qui va executer le code des champions tour
				à tour jusqu'à ce qu'un vainqueur soit. Tous les détails sur
				le fonctionnement de la VM sont décrit dans le pdf.

by Lyes Hamroun.
