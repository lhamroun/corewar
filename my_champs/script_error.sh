#!/bin/bash
make

clear
echo "\n\n\n\n\033[36mTest des .cor\033[00m"
sleep 1
declare -i I=1
while [ $I -le `ls my_champs/error/*.cor | wc -w` ]
do
	S="`./corewar my_champs/error/$I.cor | grep Error:`"
	echo "\n"`ls my_champs/error/$I.cor`
	if [ -z "$S" ]
	then
		echo "\033[31m----------------> PROBLEME <-------------------\033[00m"
	else
		echo "\033[32mBon travail\033[00m"
	fi
	sleep 0.2
	let "I=I+1"
done

sleep 2

clear
echo "\n\n\n\n\033[35mTest des .s\033[00m"
sleep 1
declare -i I=1
while [ $I -le `ls my_champs/error/*.s | wc -w` ]
do
	S="`./asm my_champs/error/$I.s | grep LADIES`"
	echo "\n" && head -n 1 my_champs/error/$I.s | cut -d "#" -f 2
	if [ -n "$S" ]
	then
		echo "\033[31m----------------> PROBLEME <-------------------\033[00m"
		echo `./asm my_champs/error/$I.s`
	else
		echo "\033[32mBon travail\033[00m"
	fi
	sleep 0.2
	let "I=I+1"
done
