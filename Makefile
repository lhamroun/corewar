# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/11/27 01:24:04 by lyhamrou          #+#    #+#              #
#    Updated: 2020/03/02 14:32:41 by lyhamrou         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME_ASM = asm
NAME_COR = corewar

COMPILE = $(CC) -Wall -Wextra -Werror #-g3 -fsanitize=address
LD_LIBS_COR = -lft -L libft/lyhamrou -lftprintf -L libft/akremer -lncurses
LD_LIBS_ASM = -lft -L libft/akremer  -lftprintf -L libft/akremer
INCLUDES = -I includes/ -I libft/lyhamrou/ -I libft/akremer/includes/
HEADER = includes/corewar.h includes/op.h includes/asm.h
LIBFT_A = libft/lyhamrou/libft.a libft/akremer/libft.a

SRC_ASM_PATH = srcs/asm/
SRC_COR_PATH = srcs/corewar/
SRC_ASM_NAME = change_label.c check_parsing2.c error.c error3.c fill2.c main.c \
				open.c parsing_instruc.c parsing_instruc_2.c swap.c \
				check_parsing.c check_parsing3.c error2.c fill.c free.c op.c \
				parsing.c parsing_instruc_1.c print.c parsing2.c
SRC_COR_NAME = main.c init.c parsing.c set_champion.c load_champion.c print.c \
				corewar.c corewar_live.c corewar_load.c corewar_aff.c \
				corewar_st.c corewar_add.c corewar_and.c corewar_sub.c \
				corewar_or.c corewar_xor.c corewar_zjmp.c corewar_ldi.c \
				corewar_sti.c corewar_fork.c corewar_lld.c corewar_lldi.c \
				corewar_lfork.c visu.c is_parsing.c corewar_and2.c \
				corewar_or2.c corewar_xor2.c exec_instructions.c \
				corewar_and3.c corewar_and4.c \
				corewar_or3.c corewar_or4.c corewar_ldi2.c \
				corewar_xor3.c corewar_xor4.c corewar_sti2.c ft_visu.c \
				ft_init_visu.c ft_init_visu2.c ft_update_col.c \
				ft_update_info.c ft_update_map.c ft_update_proc.c \
				ft_visu_header.c
SRC_ASM = $(addprefix $(SRC_ASM_PATH),$(SRC_ASM_NAME))
SRC_COR = $(addprefix $(SRC_COR_PATH),$(SRC_COR_NAME))

OBJ_PATH = .obj/
OBJ_ASM_PATH = .obj/asm/
OBJ_COR_PATH = .obj/corewar/
OBJ_ASM_NAME = $(SRC_ASM_NAME:.c=.o)
OBJ_COR_NAME = $(SRC_COR_NAME:.c=.o)
OBJ_ASM = $(addprefix $(OBJ_ASM_PATH),$(OBJ_ASM_NAME))
OBJ_COR = $(addprefix $(OBJ_COR_PATH),$(OBJ_COR_NAME))

all: $(NAME_ASM) $(NAME_COR)

$(NAME_ASM): $(OBJ_PATH) $(LIBFT_A) $(OBJ_ASM)
	$(COMPILE) -o $(NAME_ASM) $(LD_LIBS_ASM) $(INCLUDES) $(OBJ_ASM)

$(NAME_COR): $(OBJ_PATH) $(LIBFT_A) $(OBJ_COR)
	$(COMPILE) -o $(NAME_COR) $(LD_LIBS_COR) $(INCLUDES) $(OBJ_COR)

$(OBJ_PATH):
	mkdir -p $(OBJ_PATH)
	mkdir -p $(OBJ_ASM_PATH)
	mkdir -p $(OBJ_COR_PATH)

$(LIBFT_A):
	make -C libft/lyhamrou/
	make -C libft/akremer/

$(OBJ_ASM_PATH)%.o: $(SRC_ASM_PATH)%.c $(HEADER)
	$(COMPILE) $(INCLUDES) -o $@ -c $<

$(OBJ_COR_PATH)%.o: $(SRC_COR_PATH)%.c $(HEADER)
	$(COMPILE) $(INCLUDES) -o $@ -c $<

clean:
	$(RM) -rf $(OBJ_PATH)
	make clean -C libft/lyhamrou/
	make clean -C libft/akremer/

fclean: clean
	$(RM) -rf $(NAME_ASM)
	$(RM) -rf $(NAME_COR)
	make fclean -C libft/lyhamrou/
	make fclean -C libft/akremer/

re: fclean all

norme: fclean
	norminette libft/
	sleep 1
	norminette includes/
	sleep 1
	norminette srcs/

script_error: all
	sh my_champs/script_error.sh

save: fclean
	git add .
	git commit -m "auto-save"
	git push

.PHONY: libft all clean fclean re
