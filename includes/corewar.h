/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 02:11:40 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/02 13:58:42 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COREWAR_H
# define COREWAR_H

# include "op.h"
# include "libft.h"
# include "ft_printf.h"
# include <ncurses.h>

# define USAGE "Usage: ./corewar [-d (int) | -n (int) | -v] <champ1.cor> <...>"
# define NB_INSTRUCTION 17
# define SEPARATION 4
# define END_COR 0x0a
# define BLEU "\033[34m"
# define CYAN "\033[36m"
# define VERT "\033[32m"
# define JAUNE "\033[33m"
# define ROUGE "\033[31m"
# define BLANC "\033[0m"

typedef struct			s_ncu
{
	int					speed;
	int					state;
	unsigned int		cmpt;
	unsigned int		ch;
	unsigned char		*rmap;
	unsigned char		*cmap;
}						t_ncu;

typedef enum			e_error
{
	ER_CHAMPION = 1,
	ER_ARG,
	ER_MALLOC,
	ER_SIZE,
	ER_NB_PLAYERS,
	ER_EMPTY
}						t_error;
/*
**		The structure t_process contains :
**			pc    -> position of pointer
**			index -> index of champion
**			color -> color : 1 = green, 2 = red, 3 = blue, 4 = yellow
*/
typedef struct			s_visu
{
	int					pc;
	int					index;
	int					color;
	struct s_visu		*next;
}						t_visu;
/*
**		The structure t_process contains :
**			carry -> booleen
**			index -> index of the player who create the process
**			cycle -> number of cycle to wait before exec instruction
**			pc    -> position of process
**			live  -> number of live since cycle to die
*/
typedef struct			s_process
{
	unsigned char		carry:1;
	int					index;
	int					cycle;
	int					pc;
	unsigned int		live;
	int					*reg;
	struct s_process	*next;
}						t_process;
/*
**		The structure t_champion contains :
**			live  -> number of live since cycle to die
**			index -> champion number
**			code  -> champion's code
**			head  -> this struct contains champion name and size
*/
typedef struct			s_champion
{
	unsigned int		live;
	int					index;
	unsigned char		*code;
	t_header			head;
}						t_champion;
/*
**		The structure t_arena contains :
**			last  -> index of last champion who make live
**			ctd   -> value of cycle to die
**			nb_ps -> number of process
**			live  -> number of live of all champions since cycle to die
**			check -> if ctd don't change every MAX_CHECK, ctd - CYCLE_DLETA
**			cycle -> number of cycle since begining
**			map   -> the arena
**			ps -> process list
*/
typedef struct			s_arena
{
	int					last;
	int					ctd;
	unsigned int		nb_ps;
	unsigned int		live;
	unsigned int		check;
	unsigned int		cycle;
	unsigned char		*map;
	t_process			*ps;
}						t_arena;
/*
**		The structure t_env contains :
**			error -> error code for error messages (0 is no error)
**			visu  -> bool : 1 == yes && 0 == no
**			nb    -> number of players
**			d     -> dump memory
**			arena -> the struct contains the arena
**			champ -> the struct contains champions
*/
typedef struct			s_env
{
	int					error;
	unsigned int		visu;
	unsigned int		nb;
	long				d;
	t_visu				*v;
	t_ncu				ncu;
	t_arena				arena;
	t_champion			champ[MAX_PLAYERS];
}						t_env;
/*
**		Print & debug functions
*/
void					visu(t_env *env);
void					kill_visu_color(t_env *env);
int						set_visu_color(t_env *env, t_process *ps, short arg);
void					print_process(t_env *env);
void					print_arena(t_env *env);
void					print_env(t_env *env);
void					print_champion(t_env *env);
/*
**		Initialisation & free functions
*/
void					init_env(t_env *env);
int						init_arena(t_env *env);
int						init_champion(int fd, t_env *env);
t_process				*init_process(int i, t_env *env);
void					cleaner(t_env *env);
/*
**		Parsing_functions
*/
int						parsing(char **av, t_env *env);
int						is_players(char **av, t_env *env);
int						is_error(char **av, t_env *env);
int						is_dump(char **av, t_env *env);
int						is_visu(char **av, t_env *env);
int						is_players_number(char **av, t_env *env);
int						set_command(int fd, t_env *env);
int						set_magic(int fd, t_env *env);
int						set_prog_size(int fd, t_env *env);
int						set_prog_name(int fd, t_env *env);
int						set_comment(int fd, t_env *env);
int						check_same_number(t_env *env);
/*
**		Corewar functions
*/
int						load_champion(t_env *env);
void					corewar(t_env *env);
void					exec_instruction(t_env *env, t_process *ps);
/*
**		Instructions for VM
*/
int						corewar_live(t_env *env, t_process *ps);
int						corewar_load(t_env *env, t_process *ps);
int						corewar_st(t_env *env, t_process *ps);
int						corewar_add(t_env *env, t_process *ps);
int						corewar_sub(t_env *env, t_process *ps);
int						corewar_and(t_env *env, t_process *ps);
int						corewar_or(t_env *env, t_process *ps);
int						corewar_xor(t_env *env, t_process *ps);
int						corewar_zjmp(t_env *env, t_process *ps);
int						corewar_ldi(t_env *env, t_process *ps);
int						corewar_sti(t_env *env, t_process *ps);
int						corewar_fork(t_env *env, t_process *ps);
int						corewar_lld(t_env *env, t_process *ps);
int						corewar_lldi(t_env *env, t_process *ps);
int						corewar_lfork(t_env *env, t_process *ps);
int						corewar_aff(t_env *env, t_process *ps);
void					corewar_error_instructions(int i, t_env *env);
int						calc_and_5(t_env *env, t_process *ps);
int						calc_and_6(t_env *e, t_process *ps, unsigned char ocp);
int						calc_and_7(t_env *e, t_process *ps);
int						calc_and_8(t_env *e, t_process *ps, unsigned char ocp);
int						calc_and_9(t_env *e, t_process *ps, unsigned char ocp);
int						calc_and_11(t_env *e, t_process *ps);
int						calc_or_5(t_env *env, t_process *ps);
int						calc_or_6(t_env *e, t_process *ps, unsigned char ocp);
int						calc_or_7(t_env *env, t_process *ps);
int						calc_or_8(t_env *e, t_process *ps, unsigned char ocp);
int						calc_or_9(t_env *e, t_process *ps, unsigned char ocp);
int						calc_or_11(t_env *env, t_process *ps);
int						calc_xor_5(t_env *env, t_process *ps);
int						calc_xor_6(t_env *e, t_process *ps, unsigned char ocp);
int						calc_xor_7(t_env *env, t_process *ps);
int						calc_xor_8(t_env *e, t_process *ps, unsigned char ocp);
int						calc_xor_9(t_env *e, t_process *ps, unsigned char ocp);
int						calc_xor_11(t_env *env, t_process *ps);
int						calc_and_6_212(t_env *env, t_process *ps,
		unsigned char c);
int						calc_and_6_116(t_env *env, t_process *ps,
		unsigned char c);
int						calc_and_62(t_env *e, t_process *ps, unsigned char ocp,
		unsigned char c);
int						calc_and_82(t_env *env, t_process *ps,
		unsigned char ocp, unsigned char c);
int						calc_and_82_148(t_env *env, t_process *ps,
		unsigned char c);
int						calc_and_82_100(t_env *env, t_process *ps,
		unsigned char c);
void					calc_and_92(t_env *env, t_process *ps,
		unsigned char ocp, unsigned char c);
int						calc_or_62(t_env *e, t_process *ps, unsigned char ocp,
		unsigned char c);
int						calc_or_6_116(t_env *e, t_process *ps, unsigned char c);
int						calc_or_6_212(t_env *e, t_process *ps, unsigned char c);
int						calc_or_82(t_env *env, t_process *ps, unsigned char ocp,
		unsigned char c);
int						calc_or_82_148(t_env *env, t_process *ps,
		unsigned char c);
int						calc_or_82_100(t_env *env, t_process *ps,
		unsigned char c);
void					calc_or_92(t_env *env, t_process *ps, unsigned char ocp,
		unsigned char c);
void					calc_or_92_180(t_env *env, t_process *ps,
		unsigned char c);
int						calc_xor_62(t_env *e, t_process *ps, unsigned char ocp,
		unsigned char c);
int						calc_xor_6_116(t_env *e, t_process *p, unsigned char c);
int						calc_xor_6_212(t_env *e, t_process *p, unsigned char c);
int						calc_xor_82(t_env *env, t_process *p, unsigned char ocp,
		unsigned char c);
int						calc_xor_82_148(t_env *env, t_process *ps,
		unsigned char c);
int						calc_xor_82_100(t_env *env, t_process *ps,
		unsigned char c);
void					calc_xor_92(t_env *env, t_process *p, unsigned char ocp,
		unsigned char c);
void					calc_xor_92_180(t_env *env, t_process *ps,
		unsigned char c);
int						store_index_reg_addr_calc(t_process *ps,
		t_env *e, int pos, char first);
int						calc_second_sti(t_env *e, t_process *ps, int second);
void					load_index_164(t_env *env, t_process *ps,
		short *first, short *second);
void					load_index_228(t_env *env, t_process *ps, short *first,
		short *second);
void					load_index_reg_addr_calc(t_env *env,
		t_process *ps, int pos, char third);
void					ft_visu();
void					ft_init(WINDOW *info_box, WINDOW *map_corew,
		t_env *env);
void					init_colot_ncurses(void);
int						init_cmap(t_env *env);
int						init_rmap(t_env *env);
int						ft_init_map(t_env *env, WINDOW *map_corew);
void					ft_update_info(t_env *env, WINDOW *info_box);
void					ft_update_process(WINDOW *map_corew, t_env *env);
void					ft_update_color(WINDOW *map_corew, t_env *env);
void					ft_update_map(t_env *env, WINDOW *map_corew);
void					ft_header(void);

#endif
