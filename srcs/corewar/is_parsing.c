/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_parsing.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/24 15:59:47 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 02:59:36 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

/*
**			function to check if players (.cor) are well formatted
*/

int			is_players(char **av, t_env *env)
{
	int		i;
	int		fd;

	i = 1;
	while (av[i])
	{
		fd = open(av[i], O_RDONLY);
		if (fd > 2 && ft_strcmp(av[i] + ft_strlen(av[i]) - 4, ".cor") == 0)
		{
			env->champ[env->nb - 1].index = ++env->nb;
			if (env->nb <= MAX_PLAYERS)
			{
				if (init_champion(fd, env) == -1)
					return (ER_ARG);
			}
			close(fd);
		}
		fd = -1;
		++i;
	}
	return (env->nb > MAX_PLAYERS ? ER_NB_PLAYERS : 0);
}

/*
**			function to know if there is bad argument
*/

int			is_error(char **av, t_env *env)
{
	int		i;

	i = 1;
	(void)env;
	while (av[i])
	{
		if (ft_strcmp(av[i], "-dump") != 0 && ft_strcmp(av[i], "-v") != 0
			&& ft_strcmp(av[i], "-n") != 0 && str_is_number(av[i]) == 0
			&& ft_strcmp(av[i] + ft_strlen(av[i]) - 4, ".cor") != 0)
			return (ER_ARG);
		if (str_is_number(av[i]) == 1
			&& ft_strcmp(av[i - 1], "-dump") != 0
			&& ft_strcmp(av[i - 1], "-n") != 0)
			return (ER_ARG);
		++i;
	}
	return (0);
}

/*
**			function to know if there is -d option and if it-s valid argument
*/

int			is_dump(char **av, t_env *env)
{
	int		i;
	int		nb;

	i = -1;
	nb = 0;
	while (av[++i])
		if (ft_strcmp(av[i], "-dump") == 0)
			nb++;
	i = 0;
	while (av[i])
	{
		if (ft_strcmp(av[i], "-dump") == 0)
		{
			if (av[i + 1] && str_is_number(av[i + 1]))
			{
				env->d = ft_atol(av[i + 1]);
				return (env->d >= 0 && nb == 1 ? 0 : ER_ARG);
			}
			return (ER_ARG);
		}
		++i;
	}
	return (0);
}

/*
**			function to know if there is -d option and if it-s valid argument
*/

int			is_players_number(char **av, t_env *env)
{
	int		i;
	int		fd;
	int		j;

	i = 0;
	j = 0;
	while (av[++i])
	{
		if (ft_strcmp(av[i], "-n") == 0)
		{
			if (av[i + 1] && str_is_number(av[i + 1])
				&& av[i + 2] && (fd = open(av[i + 2], O_RDONLY)) != -1)
				env->champ[j++].index = ft_atoi(av[i + 1]);
			else
				return (ER_ARG);
			close(fd);
		}
		else if ((fd = open(av[i], O_RDONLY)) != -1
				&& str_is_number(av[i - 1]) == 0)
		{
			env->champ[j++].index = 0;
			close(fd);
		}
	}
	return (0);
}

/*
**			function to know if there is -n option and if it-s valid argument
*/

int			is_visu(char **av, t_env *env)
{
	int		i;
	int		nb;

	i = 0;
	nb = 0;
	while (av[i])
	{
		if (ft_strcmp(av[i], "-v") == 0)
			nb++;
		++i;
	}
	if (nb > 0)
		env->visu = 1;
	return (nb > 1 ? ER_ARG : 0);
}
