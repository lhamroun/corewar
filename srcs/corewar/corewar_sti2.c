/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_sti2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 18:36:04 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 11:52:22 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		store_index_reg_addr_calc(t_process *ps, t_env *e, int pos, char first)
{
	pos < 0 ? pos = pos + MEM_SIZE : 1;
	e->arena.map[pos % MEM_SIZE] = ps->reg[first - 1] >> 24;
	e->arena.map[(pos + 1) % MEM_SIZE] = ps->reg[first - 1] >> 16;
	e->arena.map[(pos + 2) % MEM_SIZE] = ps->reg[first - 1] >> 8;
	e->arena.map[(pos + 3) % MEM_SIZE] = ps->reg[first - 1];
	if (e->visu == 1)
		if (set_visu_color(e, ps, pos) == -1)
			return (-1);
	return (1);
}

int		calc_second_sti(t_env *env, t_process *ps, int second)
{
	second = (env->arena.map[(((ps->pc + second < 0 ? MEM_SIZE
		+ (ps->pc + second) : ps->pc + second)) % IDX_MOD) % MEM_SIZE] << 24)
		+ (env->arena.map[(((ps->pc + second + 1 < 0 ? MEM_SIZE
		+ (ps->pc + second + 1) : ps->pc + second + 1)) % IDX_MOD)
		% MEM_SIZE] << 16) + (env->arena.map[(((ps->pc + second + 2) < 0 ?
		MEM_SIZE + (ps->pc + second + 2) : ps->pc + second + 2) % IDX_MOD)
		% MEM_SIZE] << 8) + env->arena.map[(((ps->pc + second + 3) < 0 ?
		MEM_SIZE + (ps->pc + second + 3) : ps->pc + second + 3) % IDX_MOD)
		% MEM_SIZE];
	return (second);
}
