/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_lld.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/22 11:26:58 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/23 17:45:07 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static int	load_into_reg(t_env *env, t_process *ps)
{
	unsigned char	ocp;
	unsigned char	arg2;
	unsigned int	arg1;

	arg1 = 0;
	arg2 = 0;
	ocp = env->arena.map[ps->pc + 1];
	if (ocp == 144)
	{
		arg1 = ((env->arena.map[(ps->pc + 2) % MEM_SIZE]) << 24)
			+ ((env->arena.map[(ps->pc + 3) % MEM_SIZE]) << 16)
			+ ((env->arena.map[(ps->pc + 4) % MEM_SIZE]) << 8)
			+ ((env->arena.map[(ps->pc + 5) % MEM_SIZE]));
		arg2 = (env->arena.map[(ps->pc + 6) % MEM_SIZE]);
	}
	else if (ocp == 208)
	{
		arg1 = ((env->arena.map[(ps->pc + 2) % MEM_SIZE]) << 8)
			+ ((env->arena.map[(ps->pc + 3) % MEM_SIZE]));
		arg2 = (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
	}
	if ((ocp != 144 && ocp != 208) || arg2 >= REG_NUMBER)
		return (-1);
	ps->reg[arg2 - 1] = arg1;
	return (ocp == 144 && arg1 == 0 ? 1 : 0);
}

int			corewar_lld(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 8;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		ps->carry = load_into_reg(env, ps) == 1 ? 1 : 0;
		if (env->arena.map[(ps->pc + 1) % MEM_SIZE] == 208)
			ps->pc = (ps->pc + 5) % MEM_SIZE;
		else if (env->arena.map[(ps->pc + 1) % MEM_SIZE] != 208)
			ps->pc = (ps->pc + 7) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
