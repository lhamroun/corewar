/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 05:34:23 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 03:44:36 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		init_arena(t_env *env)
{
	env->arena.check = 0;
	env->arena.live = 0;
	env->arena.ctd = CYCLE_TO_DIE;
	env->arena.cycle = 0;
	if (!(env->arena.map = ft_memalloc(sizeof(unsigned char) * MEM_SIZE + 1)))
		return (-1);
	return (1);
}

void	init_env(t_env *env)
{
	ft_bzero(env, sizeof(*env));
	env->d = -1;
}

int		init_champion(int fd, t_env *env)
{
	env->champ[env->nb - 1].index = env->nb;
	if (set_magic(fd, env) != 0)
		return (-1);
	env->champ[env->nb - 1].index = env->nb;
	if (set_prog_name(fd, env) != 0)
		return (-1);
	env->champ[env->nb - 1].index = env->nb;
	lseek(fd, SEPARATION, SEEK_CUR);
	if (set_prog_size(fd, env) != 0)
		return (-1);
	env->champ[env->nb - 1].index = env->nb;
	if (set_comment(fd, env) != 0)
		return (-1);
	env->champ[env->nb - 1].index = env->nb;
	lseek(fd, SEPARATION, SEEK_CUR);
	env->champ[env->nb - 1].index = env->nb;
	if (set_command(fd, env) != 0)
		return (-1);
	return (1);
}
