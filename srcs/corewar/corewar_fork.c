/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_fork.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 14:58:25 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 05:13:20 by akremer          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

t_process	*set_new_process(t_process *ps, short x)
{
	int			j;
	int			pos;
	t_process	*tmp;

	j = 0;
	pos = 0;
	if (!(tmp = ft_memalloc(sizeof(t_process))))
		return (NULL);
	if (!(tmp->reg = ft_memalloc(sizeof(unsigned int) * REG_NUMBER)))
		return (NULL);
	while (j < REG_NUMBER)
	{
		tmp->reg[j] = ps->reg[j];
		++j;
	}
	tmp->carry = ps->carry;
	tmp->index = ps->index;
	tmp->cycle = -1;
	tmp->live = ps->live;
	pos = ps->pc + (x % IDX_MOD);
	tmp->pc = pos < 0 ? MEM_SIZE + pos : pos % MEM_SIZE;
	return (tmp);
}

int			create_process(t_process *ps, t_env *env)
{
	short		x;
	t_process	*tmp;

	x = (env->arena.map[(ps->pc + 1) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 2) % MEM_SIZE]);
	if (!(tmp = set_new_process(ps, x)))
		return (-1);
	if (env->arena.nb_ps)
		tmp->next = env->arena.ps;
	else
		tmp->next = NULL;
	env->arena.ps = tmp;
	env->arena.nb_ps++;
	return (1);
}

int			corewar_fork(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 798;
	else if (ps->cycle > 0)
		ps->cycle--;
	if (ps->cycle == 0)
	{
		if (create_process(ps, env) == -1)
			return (-1);
		ps->pc = (ps->pc + 3) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
