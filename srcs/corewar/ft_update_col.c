/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update_col.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 15:51:29 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/01 01:39:46 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void		ft_upd_col2(WINDOW *map_corew, t_env *env, int a, int c)
{
	t_visu *tmp;

	tmp = env->v;
	while (tmp)
	{
		if ((unsigned int)tmp->pc == env->ncu.cmpt)
		{
			wattron(map_corew, COLOR_PAIR(tmp->color));
			env->ncu.cmap[env->ncu.cmpt] = tmp->color;
			env->ncu.cmap[env->ncu.cmpt + 1] = tmp->color;
			env->ncu.cmap[env->ncu.cmpt + 2] = tmp->color;
			env->ncu.cmap[env->ncu.cmpt + 3] = tmp->color;
			env->ncu.rmap[env->ncu.cmpt] = env->arena.map[env->ncu.cmpt];
			if ((env->arena.map[env->ncu.cmpt] >= 0 &&
						env->arena.map[env->ncu.cmpt] < 16))
				mvwprintw(map_corew, a, c + 3, "0%x",
						env->ncu.rmap[env->ncu.cmpt]);
			else
				mvwprintw(map_corew, a, c + 3, "%x",
						env->ncu.rmap[env->ncu.cmpt]);
			wattroff(map_corew, COLOR_PAIR(tmp->color));
			break ;
		}
		tmp = tmp->next;
	}
}

void		ft_update_color(WINDOW *map_corew, t_env *env)
{
	int		a;
	int		c;

	a = 2;
	c = 1;
	env->ncu.cmpt = 0;
	while (++a < 67)
	{
		while (c < 192)
		{
			ft_upd_col2(map_corew, env, a, c);
			env->ncu.cmpt += 1;
			c += 3;
		}
		c = 1;
	}
}
