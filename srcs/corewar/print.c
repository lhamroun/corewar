/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/29 02:17:41 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/13 12:00:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		arena_find_index(t_env *env, int i)
{
	int					b;
	t_process			*ps;

	b = 0;
	ps = env->arena.ps;
	while (ps)
	{
		if (i - 1 == ps->pc)
		{
			b = ps->index;
			break ;
		}
		ps = ps->next;
	}
	return (b);
}

void	print_char_arena(t_env *env, int b, short x)
{
	if (b == env->champ[0].index)
		ft_printf(VERT"%x"BLANC, x);
	else if (env->nb > 1 && b == env->champ[1].index)
		ft_printf(ROUGE"%x"BLANC, x);
	else if (env->nb > 2 && b == env->champ[2].index)
		ft_printf(BLEU"%x"BLANC, x);
	else if (env->nb > 3 && b == env->champ[3].index)
		ft_printf(JAUNE"%x"BLANC, x);
	else
		ft_printf(BLANC"%x"BLANC, x);
}

void	print_arena(t_env *env)
{
	short				x;
	int					b;
	int					i;

	i = 1;
	ft_printf("PRINT_ARENA ---------------------------------\n");
	ft_printf("cycle             -----> %u\n", env->arena.cycle);
	ft_printf("nombre de process -----> %u\n", env->arena.nb_ps);
	while (i <= MEM_SIZE)
	{
		b = arena_find_index(env, i);
		ft_printf(" ");
		x = env->arena.map[i - 1];
		x = x >> 4;
		print_char_arena(env, b, x);
		x = env->arena.map[i - 1];
		x = x >> 4 | x << 4;
		x = x >> 4;
		x = x % 16;
		print_char_arena(env, b, x);
		if (i % 64 == 0)
			ft_printf("\n");
		++i;
	}
}

void	print_env(t_env *env)
{
	int		i;

	i = 0;
	ft_printf("PRINT_ENV --------------------------------------------------\n");
	ft_printf("	- nombre de joueurs : %d\n", env->nb);
	ft_printf("	- option(s) :\n");
	if (env->d != -1)
		ft_printf("		-d : %ld\n", env->d);
	if (env->visu == 1)
		ft_printf("		-v\n");
	if (env->error)
		ft_printf(" -nombre de l'erreur : %d\n", env->error);
	ft_printf("\n--------------------------------------------------------\n\n");
}

void	print_champion(t_env *env)
{
	unsigned int	i;
	int				j;
	t_champion		champ;

	i = 0;
	while (i < env->nb && i < MAX_PLAYERS)
	{
		j = 0;
		champ = env->champ[i];
		ft_printf("|	name	  -->	%s\n", champ.head.prog_name);
		ft_printf("|	index     -->	%u\n", champ.index);
		ft_printf("|	prog_size -->	%u\n", champ.head.prog_size);
		ft_printf("|	comment   -->	%s\n", champ.head.comment);
		ft_printf("|	code :\n");
		while (j < (int)env->champ[i].head.prog_size && j < 7)
			ft_printf("|		%x\n", champ.code[j++]);
		if (j == 7)
			if (env->champ[i].head.prog_size > 7)
			{
				ft_printf("|	...\n");
				ft_printf("|	%x\n",
					champ.code[env->champ[i].head.prog_size - 1]);
			}
		++i;
	}
}
