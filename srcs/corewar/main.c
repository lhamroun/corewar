/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 01:57:28 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/02 14:31:20 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

/*
**	__attribute__((destructor)) void	check_leaks(void)
**	{while(1);}
*/

void		cleaner(t_env *env)
{
	unsigned int	i;
	t_process		*ps;
	t_process		*tmp;

	i = 0;
	if (env->arena.map)
		ft_strdel((char **)&env->arena.map);
	ps = env->arena.ps;
	while (i < env->nb)
	{
		if (env->champ[i].code)
			ft_strdel((char **)&env->champ[i].code);
		++i;
	}
	while (ps)
	{
		tmp = ps->next;
		free(ps->reg);
		ps->reg = NULL;
		free(ps);
		ps = NULL;
		ps = tmp;
	}
	exit(0);
}

static int	error_arg(t_env *env, int b)
{
	unsigned int	i;
	static char		*msg[6] = {
	"Error: File .cor is corrupted", "Error: Definition of arguments",
	"Error: Malloc", "Error: one champion is too large",
	"Error: number of players", "error: Empty file"};

	i = 0;
	write(1, msg[env->error - 1], ft_strlen(msg[env->error - 1]));
	write(1, "\n", 1);
	if (b == 1)
		cleaner(env);
	while (i < MAX_PLAYERS)
	{
		if (i < env->nb && env->champ[i].code)
			ft_strdel((char **)&env->champ[i].code);
		++i;
	}
	return (0);
}

static int	ft_usage(void)
{
	ft_putendl(USAGE);
	return (0);
}

void		print_winner(t_env *env)
{
	int				i;

	i = 0;
	while (i < (int)env->nb && env->champ[i].index != env->arena.last)
	{
		if (env->champ[i].index == env->arena.last)
			break ;
		++i;
	}
	if (env->visu == 0)
		ft_printf("le joueur %d(%s) a gagne", env->arena.last,
			env->champ[i].head.prog_name);
}

int			main(int ac, char **av)
{
	t_env			env;

	if (ac < 2)
		return (ft_usage());
	init_env(&env);
	if (parsing(av, &env) == -1)
		return (error_arg(&env, 0));
	env.arena.last = env.champ[0].index;
	if (init_arena(&env) == -1)
		return (1);
	if ((env.error = load_champion(&env)) != 0)
		return (error_arg(&env, 1));
	if (env.error != 0)
		return (error_arg(&env, 1));
	corewar(&env);
	print_winner(&env);
	cleaner(&env);
	return (0);
}
