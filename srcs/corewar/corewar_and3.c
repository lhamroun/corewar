/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_and3.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 13:47:01 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:19:41 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		calc_and_6_212(t_env *env, t_process *ps, unsigned char c)
{
	short	a;
	short	b;

	a = 0;
	b = 0;
	a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE]);
	b = env->arena.map[(ps->pc + 4) % MEM_SIZE];
	if (!b || b > REG_NUMBER)
		return (11);
	b = ps->reg[b - 1];
	a = env->arena.map[(ps->pc + a) % MEM_SIZE];
	ps->reg[c - 1] = a & b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (1);
}

int		calc_and_6_116(t_env *env, t_process *ps, unsigned char c)
{
	short	a;
	short	b;

	a = 0;
	b = 0;
	a = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	if (!a || a > REG_NUMBER)
		return (11);
	a = ps->reg[a - 1];
	b = (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
	b = env->arena.map[(ps->pc + b) % MEM_SIZE];
	ps->reg[c - 1] = a & b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (1);
}

int		calc_and_62(t_env *e, t_process *ps, unsigned char ocp, unsigned char c)
{
	if (ocp == 116)
	{
		if (calc_and_6_116(e, ps, c) == 11)
			return (11);
	}
	else if (ocp == 212)
	{
		if (calc_and_6_212(e, ps, c) == 11)
			return (11);
	}
	return (1);
}
