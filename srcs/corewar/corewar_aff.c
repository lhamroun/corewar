/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_aff.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:07:40 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/02 13:53:57 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void	aff_reg(t_env *env, t_process *ps)
{
	char			ocp;
	unsigned char	reg;

	ocp = env->arena.map[(ps->pc + 1) % MEM_SIZE];
	reg = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	if (env->visu == 0)
	{
		ft_putchar(ps->reg[reg] % 256);
		ft_putchar('\n');
	}
	if (ocp != 64 || reg < 1 || reg > REG_NUMBER)
		return ;
}

int		corewar_aff(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 1;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		aff_reg(env, ps);
		ps->pc = (ps->pc + 3) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
