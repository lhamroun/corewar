/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_or.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 15:29:00 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:16:48 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		calc_or_5(t_env *env, t_process *ps)
{
	unsigned char	a;
	unsigned char	b;
	unsigned char	c;

	a = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	b = env->arena.map[(ps->pc + 3) % MEM_SIZE];
	c = env->arena.map[(ps->pc + 4) % MEM_SIZE];
	if (!a || !b || !c || a > REG_NUMBER || b > REG_NUMBER || c > REG_NUMBER)
		return (11);
	ps->reg[c - 1] = ps->reg[a - 1] | ps->reg[b - 1];
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (5);
}

int		calc_or_6(t_env *env, t_process *ps, unsigned char ocp)
{
	unsigned char	c;

	c = env->arena.map[(ps->pc + 5) % MEM_SIZE];
	if (!c || c > REG_NUMBER)
		return (11);
	if (calc_or_62(env, ps, ocp, c) == 11)
		return (11);
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (6);
}

int		calc_or_7(t_env *env, t_process *ps)
{
	short			a;
	short			b;
	unsigned char	c;

	a = 0;
	b = 0;
	c = env->arena.map[(ps->pc + 6) % MEM_SIZE];
	if (!c || c > REG_NUMBER)
		return (11);
	a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE]);
	b = (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	a = env->arena.map[(ps->pc + a) % MEM_SIZE];
	b = env->arena.map[(ps->pc + b) % MEM_SIZE];
	ps->reg[c - 1] = a | b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (7);
}

int		or_operation(t_env *env, t_process *ps)
{
	unsigned char	ocp;

	ocp = env->arena.map[(ps->pc + 1) % MEM_SIZE];
	if (ocp == 84)
		return (calc_or_5(env, ps));
	else if (ocp == 212 || ocp == 116)
		return (calc_or_6(env, ps, ocp));
	else if (ocp == 244)
		return (calc_or_7(env, ps));
	else if (ocp == 100 || ocp == 148)
		return (calc_or_8(env, ps, ocp));
	else if (ocp == 180 || ocp == 228)
		return (calc_or_9(env, ps, ocp));
	else if (ocp == 164)
		return (calc_or_11(env, ps));
	else
		ps->carry = 0;
	return (11);
}

int		corewar_or(t_env *env, t_process *ps)
{
	int		x;

	x = 0;
	if (ps->cycle == -1)
		ps->cycle = 4;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		x = or_operation(env, ps);
		ps->pc = (ps->pc + x) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
