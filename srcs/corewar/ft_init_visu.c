/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_visu.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 13:14:04 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/02 13:07:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void		ft_init(WINDOW *info_box, WINDOW *map_corew, t_env *env)
{
	env->ncu.speed = 1;
	env->ncu.cmpt = 0;
	env->ncu.ch = 0;
	wattron(info_box, A_BOLD);
	ft_header();
	mvwprintw(info_box, 20, 20, "ACTUAL SPEED = ");
	mvwprintw(info_box, 20, 39, "%d cycle/s", env->ncu.speed);
	mvwprintw(info_box, 25, 30, "CHAMPIONS :");
	mvwprintw(info_box, 50, 20, "CONTROLS :");
	mvwprintw(info_box, 52, 20, "p : +10 CYCLES/S");
	mvwprintw(info_box, 54, 20, "l : -10 CYCLES/S");
	mvwprintw(info_box, 56, 20, "^ : +1  CYCLE/S");
	mvwprintw(info_box, 58, 20, "v : -1  CYCLE/S");
	mvwprintw(info_box, 61, 20, "SPACE TO SET PAUSE");
	mvwprintw(info_box, 63, 20, "ESC TO QUIT");
	wattroff(info_box, A_BOLD);
	wborder(map_corew, '|', '[', '_', '_', ' ', '_', ' ', '_');
	wborder(info_box, ']', '|', '_', '_', '_', ' ', '_', ' ');
}

void		init_colot_ncurses(void)
{
	initscr();
	timeout(TRUE);
	start_color();
	cbreak();
	noecho();
	curs_set(0);
	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_CYAN, COLOR_BLACK);
	init_pair(4, COLOR_MAGENTA, COLOR_BLACK);
	init_pair(5, COLOR_BLACK, COLOR_GREEN);
	init_pair(6, COLOR_BLACK, COLOR_RED);
	init_pair(7, COLOR_BLACK, COLOR_CYAN);
	init_pair(8, COLOR_BLACK, COLOR_MAGENTA);
}

int			init_cmap(t_env *env)
{
	int		i;
	int		j;
	int		cpt;

	j = 0;
	cpt = MEM_SIZE / env->nb;
	if (!(env->ncu.cmap = ft_memalloc(sizeof(char *) * MEM_SIZE + 1)))
	{
		env->ncu.state = 2;
		return (-1);
	}
	while (j < (int)env->nb)
	{
		i = 0;
		while (i < cpt)
		{
			if (i < (int)env->champ[j].head.prog_size)
				env->ncu.cmap[i + j * cpt] = j + 1;
			++i;
		}
		++j;
	}
	return (0);
}

int			init_rmap(t_env *env)
{
	if (!(env->ncu.rmap = ft_memalloc(sizeof(char) * MEM_SIZE + 1)))
	{
		env->ncu.cmap ? free(env->ncu.cmap) : 1;
		env->ncu.state = 2;
		return (-1);
	}
	ft_memcpy(env->ncu.rmap, env->arena.map, MEM_SIZE);
	return (0);
}
