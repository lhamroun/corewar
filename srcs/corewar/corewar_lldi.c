/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_lldi.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 16:03:15 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/23 17:45:17 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static void	lload_index_reg(t_process *ps, t_env *env)
{
	char	first;
	char	second;
	char	third;
	int		pos;

	first = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	second = env->arena.map[(ps->pc + 3) % MEM_SIZE];
	third = env->arena.map[(ps->pc + 4) % MEM_SIZE];
	if (first < 1 || first > REG_NUMBER || second < 1 || second > REG_NUMBER
		|| third < 1 || third > REG_NUMBER)
		return ;
	pos = (ps->reg[first - 1] + ps->reg[second - 1]) % IDX_MOD;
	pos < 0 ? pos = pos + MEM_SIZE : 1;
	ps->reg[third - 1] = (env->arena.map[pos % MEM_SIZE] << 24)
		+ (env->arena.map[(pos + 1) % MEM_SIZE] << 16)
		+ (env->arena.map[(pos + 2) % MEM_SIZE] << 8)
		+ env->arena.map[(pos + 3) % MEM_SIZE];
}

static void	lload_index_reg_addr(t_process *ps, t_env *e, short ocp, char third)
{
	short	first;
	short	second;
	int		pos;

	first = 0;
	second = 0;
	if (ocp == 148 || ocp == 212)
	{
		first = (e->arena.map[(ps->pc + 2) % MEM_SIZE] << 8)
			+ (e->arena.map[(ps->pc + 3) % MEM_SIZE]);
		if (ocp == 212)
			first = e->arena.map[((ps->pc + first) % MEM_SIZE) % MEM_SIZE];
		second = e->arena.map[(ps->pc + 4) % MEM_SIZE];
		if (second < 1 || second > REG_NUMBER)
			return ;
		second = ps->reg[second - 1];
	}
	else if (ocp == 100)
	{
		first = e->arena.map[(ps->pc + 2) % MEM_SIZE];
		second = (e->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
			+ (e->arena.map[(ps->pc + 4) % MEM_SIZE]);
	}
	pos = (ps->pc + first + second) % MEM_SIZE;
	load_index_reg_addr_calc(e, ps, pos, third);
}

static void	lload_index_addr(t_process *ps, t_env *env, short ocp)
{
	short	first;
	short	second;
	char	third;
	int		pos;

	first = 0;
	second = 0;
	third = env->arena.map[(ps->pc + 6) % MEM_SIZE];
	if (third < 1 || third > REG_NUMBER)
		return ;
	first = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE]);
	second = (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	if (ocp == 228)
		first = env->arena.map[(ps->pc + first) % MEM_SIZE];
	pos = (ps->pc + first + second) % MEM_SIZE;
	pos < 0 ? pos = pos + MEM_SIZE : 1;
	ps->reg[third - 1] = (env->arena.map[pos % MEM_SIZE] << 24)
		+ (env->arena.map[(pos + 1) % MEM_SIZE] << 16)
		+ (env->arena.map[(pos + 2) % MEM_SIZE] << 8)
		+ (env->arena.map[(pos + 3) % MEM_SIZE]);
}

static char	lload_index(t_process *ps, t_env *env)
{
	short		ocp;
	char		third;

	ocp = env->arena.map[(ps->pc + 1) % MEM_SIZE];
	third = 0;
	if (ocp == 84)
	{
		lload_index_reg(ps, env);
		return (3);
	}
	else if (ocp == 100 || ocp == 212 || ocp == 148)
	{
		third = env->arena.map[(ps->pc + 5) % MEM_SIZE];
		if (third < 1 || third > REG_NUMBER)
			return (4);
		lload_index_reg_addr(ps, env, ocp, third);
		return (4);
	}
	else if (ocp == 228 || ocp == 164)
	{
		lload_index_addr(ps, env, ocp);
		return (5);
	}
	return (-1);
}

int			corewar_lldi(t_env *env, t_process *ps)
{
	char		x;

	x = -1;
	if (ps->cycle == -1)
		ps->cycle = 23;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		x = lload_index(ps, env);
		if (x == 3)
			ps->pc = (ps->pc + 5) % MEM_SIZE;
		else if (x == 4)
			ps->pc = (ps->pc + 6) % MEM_SIZE;
		else
			ps->pc = (ps->pc + 7) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
