/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_ldi2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 15:56:46 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/01 16:00:33 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void	load_index_reg_addr_calc(t_env *env, t_process *ps, int pos, char third)
{
	pos < 0 ? pos = pos + MEM_SIZE : 1;
	ps->reg[third - 1] = (env->arena.map[pos % MEM_SIZE] << 24)
		+ (env->arena.map[(pos + 1) % MEM_SIZE] << 16)
		+ (env->arena.map[(pos + 2) % MEM_SIZE] << 8)
		+ (env->arena.map[(pos + 3) % MEM_SIZE]);
}
