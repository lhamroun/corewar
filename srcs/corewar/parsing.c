/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/27 05:48:42 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 03:04:48 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int
	check_if_champ_number_is_valid(unsigned int *i, int *n, int b, t_env *env)
{
	if (b == 1)
	{
		*n += 1;
		b = 0;
	}
	else if (b > 1)
		return (ER_ARG);
	else if (b == 0)
	{
		*i = 0;
		while (*i < env->nb)
		{
			if (env->champ[*i].index == 0)
			{
				env->champ[*i].index = *n;
				*n += 1;
				break ;
			}
			*i = *i + 1;
		}
	}
	return (0);
}

int
	check_doublon(t_env *env)
{
	unsigned int	i;
	unsigned int	j;
	int				n;

	i = 0;
	while (i < env->nb)
	{
		j = i + 1;
		n = env->champ[i].index;
		while (j < env->nb)
		{
			if (n == env->champ[j].index)
				return (ER_ARG);
			++j;
		}
		++i;
	}
	return (0);
}

/*
**			check data inside champion
*/

int
	check_champ_number(t_env *env)
{
	int				n;
	int				b;
	unsigned int	x;
	unsigned int	i;

	n = 1;
	x = 0;
	while (x < env->nb)
	{
		i = 0;
		b = 0;
		while (i < env->nb)
		{
			if (env->champ[i].index == n)
				++b;
			++i;
		}
		if (check_if_champ_number_is_valid(&i, &n, b, env) == -1)
			return (ER_ARG);
		++x;
	}
	if (check_doublon(env) == -1)
		return (ER_ARG);
	return (0);
}

int
	check_champ(t_env *env)
{
	unsigned int	i;

	i = 0;
	if (env->nb > MAX_PLAYERS)
		return (ER_NB_PLAYERS);
	else if (env->nb < 1)
		return (ER_NB_PLAYERS);
	while (i < env->nb)
	{
		if (env->champ[i].head.magic != COREWAR_EXEC_MAGIC)
			return (ER_CHAMPION);
		++i;
	}
	if (check_champ_number(env) == -1)
		return (ER_ARG);
	return (0);
}

/*
**			parsing function
*/

int
	parsing(char **av, t_env *env)
{
	int				i;

	i = 1;
	if ((env->error = is_dump(av, env)) != 0)
		return (-1);
	if ((env->error = is_visu(av, env)) != 0)
		return (-1);
	if ((env->error = is_error(av, env)) != 0)
		return (-1);
	if ((env->error = is_players(av, env)) != 0)
		return (-1);
	if ((env->error = is_players_number(av, env)) != 0)
		return (-1);
	if ((env->error = check_champ(env)) != 0)
		return (-1);
	if ((env->error = check_same_number(env)) != 0)
		return (-1);
	return (1);
}
