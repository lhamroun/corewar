/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_xor2.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 15:33:45 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:17:54 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		calc_xor_8(t_env *env, t_process *ps, unsigned char ocp)
{
	int				a;
	int				b;
	unsigned char	c;

	a = 0;
	b = 0;
	c = env->arena.map[(ps->pc + 7) % MEM_SIZE];
	if (!c || c > REG_NUMBER)
		return (11);
	if (calc_xor_82(env, ps, ocp, c) == 11)
		return (11);
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (8);
}

int		calc_xor_9(t_env *env, t_process *ps, unsigned char ocp)
{
	int				a;
	int				b;
	unsigned char	c;

	a = 0;
	b = 0;
	c = env->arena.map[(ps->pc + 8) % MEM_SIZE];
	if (!c || c > REG_NUMBER)
		return (11);
	calc_xor_92(env, ps, ocp, c);
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (9);
}

int		calc_xor_11(t_env *env, t_process *ps)
{
	int				a;
	int				b;
	unsigned char	c;

	a = 0;
	b = 0;
	c = env->arena.map[(ps->pc + 10) % MEM_SIZE];
	if (!c || c > REG_NUMBER)
		return (11);
	a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	b = (env->arena.map[(ps->pc + 6) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 7) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 8) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 9) % MEM_SIZE]);
	ps->reg[c - 1] = a ^ b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (11);
}
