/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update_map.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 16:02:40 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/01 01:39:58 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void		ft_upd_map2(t_env *env, WINDOW *map_corew, int a, int c)
{
	if (env->ncu.cmap[env->ncu.cmpt] != 0)
		wattron(map_corew, COLOR_PAIR(env->ncu.cmap[env->ncu.cmpt]));
	env->ncu.rmap[env->ncu.cmpt] = env->arena.map[env->ncu.cmpt];
	if ((env->arena.map[env->ncu.cmpt] >= 0 &&
				env->arena.map[env->ncu.cmpt] < 16))
		mvwprintw(map_corew, a, c + 3, "0%x",
				env->ncu.rmap[env->ncu.cmpt]);
	else
		mvwprintw(map_corew, a, c + 3, "%x",
				env->ncu.rmap[env->ncu.cmpt]);
	if (env->ncu.cmap[env->ncu.cmpt] != 0)
		wattroff(map_corew, COLOR_PAIR(env->ncu.cmap[env->ncu.cmpt]));
	env->ncu.cmpt += 1;
}

void		ft_update_map(t_env *env, WINDOW *map_corew)
{
	int		a;
	int		c;

	a = 2;
	c = 1;
	env->ncu.cmpt = 0;
	while (++a < 67)
	{
		while (c < 192)
		{
			ft_upd_map2(env, map_corew, a, c);
			c += 3;
		}
		c = 1;
	}
	ft_update_process(map_corew, env);
	ft_update_color(map_corew, env);
	wrefresh(map_corew);
}
