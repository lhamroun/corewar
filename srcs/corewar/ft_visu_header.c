/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_visu_header.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/03/02 13:06:04 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/02 14:22:08 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void	ft_display_co(int i)
{
	int		j;

	j = 10;
	mvprintw(12, i, "########");
	mvprintw(13, i, "##");
	mvprintw(14, i, "##");
	mvprintw(15, i, "##");
	mvprintw(16, i, "##");
	mvprintw(17, i, "##");
	mvprintw(18, i, "#######");
	mvprintw(12, i + j, "#########");
	mvprintw(13, i + j, "##     ##");
	mvprintw(14, i + j, "##     ##");
	mvprintw(15, i + j, "##     ##");
	mvprintw(16, i + j, "##     ##");
	mvprintw(17, i + j, "##     ##");
	mvprintw(18, i + j, "#########");
}

void	ft_display_re(int i)
{
	int		j;

	j = 11;
	mvprintw(12, i, "########");
	mvprintw(13, i, "##    ##");
	mvprintw(14, i, "##    ##");
	mvprintw(15, i, "#######");
	mvprintw(16, i, "#####");
	mvprintw(17, i, "##   ##");
	mvprintw(18, i, "##    ##");
	mvprintw(12, i + j, "########");
	mvprintw(13, i + j, "##      ");
	mvprintw(14, i + j, "##      ");
	mvprintw(15, i + j, "###### ");
	mvprintw(16, i + j, "##      ");
	mvprintw(17, i + j, "##      ");
	mvprintw(18, i + j, "########");
}

void	ft_display_wa(int i)
{
	int		j;

	j = 14;
	mvprintw(12, i, "##        ##");
	mvprintw(13, i, "##        ##");
	mvprintw(14, i, "##        ##");
	mvprintw(15, i, "##   ##   ##");
	mvprintw(16, i, "##   ##   ##");
	mvprintw(17, i, "##   ##   ##");
	mvprintw(18, i, "  ###  ###");
	mvprintw(12, i + j, "#########");
	mvprintw(13, i + j, "##     ##");
	mvprintw(14, i + j, "##     ##");
	mvprintw(15, i + j, "#########");
	mvprintw(16, i + j, "##     ##");
	mvprintw(17, i + j, "##     ##");
	mvprintw(18, i + j, "##     ##");
}

void	ft_display_r(int i)
{
	int		j;

	j = 12;
	mvprintw(12, i, "########");
	mvprintw(13, i, "##    ##");
	mvprintw(14, i, "##    ##");
	mvprintw(15, i, "#######");
	mvprintw(16, i, "#####");
	mvprintw(17, i, "##   ##");
	mvprintw(18, i, "##    ##");
	mvprintw(12, i + j, "##");
	mvprintw(13, i + j, "##");
	mvprintw(14, i + j, "## ");
	mvprintw(15, i + j, "##");
	mvprintw(16, i + j, "##");
	mvprintw(17, i + j, "  ");
	mvprintw(18, i + j, "##");
}

void	ft_header(void)
{
	ft_display_co(225);
	ft_display_re(246);
	ft_display_wa(267);
	ft_display_r(292);
}
