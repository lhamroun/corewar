/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_visu.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/21 01:03:31 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/02 14:04:32 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

int			verif(t_env *env, WINDOW *map_corew, WINDOW *info_box)
{
	if (MEM_SIZE != 4096 || env->ncu.state == 2)
	{
		env->visu = 0;
		delwin(map_corew);
		delwin(info_box);
		endwin();
		return (2);
	}
	return (0);
}

void		ft_visu(t_env *env)
{
	WINDOW	*map_corew;
	WINDOW	*info_box;

	map_corew = NULL;
	info_box = NULL;
	init_colot_ncurses();
	if (verif(env, map_corew, info_box))
		return ;
	keypad(stdscr, TRUE);
	info_box = subwin(stdscr, LINES - 10, ((COLS - 10) / 2) - 24, 5, 206);
	map_corew = subwin(stdscr, LINES - 10, ((COLS - 10) / 2) + 24, 5, 5);
	if (env->arena.cycle == 0)
	{
		ft_init(info_box, map_corew, env);
		ft_init_map(env, map_corew);
	}
	else
		ft_update_map(env, map_corew);
	ft_update_info(env, info_box);
	wrefresh(info_box);
	if (env->ncu.speed != 500)
		usleep(1000000 / env->ncu.speed);
}
