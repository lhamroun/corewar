/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update_info.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 15:01:57 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/01 09:34:14 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void		ft_info_up(WINDOW *info_box, t_env *env)
{
	wattron(info_box, A_BOLD);
	mvwprintw(info_box, 22, 20, "NUMBER OF PROCESSES = %d", env->arena.nb_ps);
	mvwprintw(info_box, 42, 20, "CYCLE		%u", env->arena.cycle);
	mvwprintw(info_box, 44, 20, "CYCLE TO DIE	%d      ",
	env->arena.ctd);
	wattroff(info_box, A_BOLD);
	env->ncu.ch = 0;
}

void		ft_info_change(t_env *env, WINDOW *info_box, int key)
{
	if (key == KEY_UP)
		env->ncu.speed += 1;
	else if (key == KEY_DOWN)
		env->ncu.speed -= 1;
	else if (key == 112)
		env->ncu.speed += 10;
	else if (key == 108 && env->ncu.speed - 10 >= 1)
		env->ncu.speed -= 10;
	else if (key == 32)
		while ((key = getch()) != 32)
			;
	if (env->ncu.speed < 1)
		env->ncu.speed = 1;
	if (env->ncu.speed > 500)
		env->ncu.speed = 500;
	if (key == 27)
		env->ncu.state = 2;
	if (key == 108 || key == 112 || key == KEY_UP || key == KEY_DOWN)
	{
		wattron(info_box, A_BOLD);
		mvwprintw(info_box, 20, 39, "%d cycle/s   ", env->ncu.speed);
		wattroff(info_box, A_BOLD);
	}
}

void		ft_update_info(t_env *env, WINDOW *info_box)
{
	int		key;

	if ((key = getch()))
		ft_info_change(env, info_box, key);
	while (env->ncu.ch < env->nb)
	{
		wattron(info_box, COLOR_PAIR(env->ncu.ch + 1));
		mvwprintw(info_box, 29 + env->ncu.ch * 2, 30, "%s",
		env->champ[env->ncu.ch].head.prog_name);
		mvwprintw(info_box, 30 + env->ncu.ch * 2, 30, "NBR LIVE = %u    ",
		env->champ[env->ncu.ch].live);
		wattroff(info_box, COLOR_PAIR(env->ncu.ch + 1));
		env->ncu.ch++;
	}
	ft_info_up(info_box, env);
}
