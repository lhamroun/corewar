/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_add.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:07:44 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:14:56 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static void	operation_add(t_env *env, t_process *ps)
{
	char			ocp;
	unsigned char	a;
	unsigned char	b;
	unsigned char	c;

	ocp = env->arena.map[(ps->pc + 1) % MEM_SIZE];
	a = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	b = env->arena.map[(ps->pc + 3) % MEM_SIZE];
	c = env->arena.map[(ps->pc + 4) % MEM_SIZE];
	if (!a || !b || !c || a > REG_NUMBER || b > REG_NUMBER || c > REG_NUMBER
		|| ocp != 84)
	{
		ps->carry = 0;
		return ;
	}
	ps->reg[c - 1] = ps->reg[a - 1] + ps->reg[b - 1];
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
}

int			corewar_add(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 8;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		operation_add(env, ps);
		ps->pc = (ps->pc + 5) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
