/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_load.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:16:48 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:54:54 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static int	load_into_reg(t_env *env, t_process *ps, unsigned char ocp)
{
	unsigned char	arg2;
	int				arg1;

	arg1 = 0;
	arg2 = 0;
	if (ocp == 144)
	{
		arg1 = ((env->arena.map[(ps->pc + 2) % MEM_SIZE]) << 24)
			+ ((env->arena.map[(ps->pc + 3) % MEM_SIZE]) << 16)
			+ ((env->arena.map[(ps->pc + 4) % MEM_SIZE]) << 8)
			+ ((env->arena.map[(ps->pc + 5) % MEM_SIZE]));
		arg2 = (env->arena.map[(ps->pc + 6) % MEM_SIZE]);
	}
	else if (ocp == 208)
	{
		arg1 = (((env->arena.map[(ps->pc + 2) % MEM_SIZE]) << 8)
			+ ((env->arena.map[(ps->pc + 3) % MEM_SIZE])) % IDX_MOD);
		arg2 = (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
		arg1 = env->arena.map[ps->pc + arg1];
	}
	if ((ocp != 144 && ocp != 208) || arg2 > REG_NUMBER || !arg2)
		return (-1);
	ps->reg[arg2 - 1] = arg1;
	return (ocp == 144 && arg1 == 0 ? 1 : 0);
}

int			corewar_load(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 3;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		ps->carry = load_into_reg(env, ps, env->arena.map[ps->pc + 1]) == 1
			? 1 : 0;
		if (env->arena.map[(ps->pc + 1) % MEM_SIZE] == 208)
			ps->pc = (ps->pc + 5) % MEM_SIZE;
		else if (env->arena.map[(ps->pc + 1) % MEM_SIZE] != 208)
			ps->pc = (ps->pc + 7) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
