/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_live.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/09 17:00:54 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/23 17:44:56 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void	print_live_message(int j, int x, t_env *env)
{
	if (env->visu == 0)
	{
		ft_printf("un processus dit que le joueur %d(%s) est en vie\n",
			x, env->champ[j].head.prog_name);
	}
}

void	find_who_live(t_env *env, t_process *ps)
{
	int			j;
	int			x;

	j = 0;
	x = (env->arena.map[(ps->pc + 1) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
	while (j < (int)env->nb)
	{
		if (env->champ[j].index == x)
		{
			env->champ[j].live++;
			env->arena.last = env->champ[j].index;
			print_live_message(j, x, env);
			break ;
		}
		++j;
	}
}

int		corewar_live(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 8;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		find_who_live(env, ps);
		env->arena.live++;
		ps->live++;
		ps->pc = (ps->pc + 5) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
