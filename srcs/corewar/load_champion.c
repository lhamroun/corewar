/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   load_champion.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 07:43:44 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:37:07 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int			check_same_number(t_env *env)
{
	unsigned int	i;
	unsigned int	j;

	i = 0;
	while (i < env->nb - 1)
	{
		j = i + 1;
		while (j < env->nb)
		{
			if (env->champ[i].index == env->champ[j].index)
				return (ER_ARG);
			++j;
		}
		++i;
	}
	return (0);
}

t_process	*init_process(int i, t_env *env)
{
	int				x;
	t_process		*ps;

	x = 1;
	if (!(ps = ft_memalloc(sizeof(t_process))))
		return (NULL);
	if (!(ps->reg = ft_memalloc(sizeof(REG_SIZE) * REG_NUMBER)))
		return (NULL);
	while (x < REG_NUMBER)
		ps->reg[x++] = 0;
	ps->reg[0] = (int)env->champ[i].index;
	ps->pc = i * (MEM_SIZE / env->nb);
	ps->index = env->champ[i].index;
	ps->live = 0;
	ps->carry = 0;
	ps->cycle = -1;
	ps->next = NULL;
	return (ps);
}

int			load_champion(t_env *env)
{
	unsigned int		i;
	t_process			*tmp;

	i = 0;
	env->arena.ps = NULL;
	while (i < env->nb)
	{
		if ((tmp = init_process(i, env)) == NULL)
			return (ER_MALLOC);
		tmp->next = env->arena.ps;
		env->arena.ps = tmp;
		if (env->champ[i].head.prog_size > (unsigned int)CHAMP_MAX_SIZE)
			return (ER_SIZE);
		ft_memcpy(env->arena.map + (i * (MEM_SIZE / env->nb)),
			env->champ[i].code,
			sizeof(unsigned char) * env->champ[i].head.prog_size);
		++i;
	}
	env->arena.nb_ps = env->nb;
	return (0);
}
