/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_champion.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/03 03:42:03 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 11:09:50 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

int		set_comment(int fd, t_env *env)
{
	unsigned char	*buf;

	if (!(buf = (unsigned char *)ft_memalloc(sizeof(char)
		* COMMENT_LENGTH + 1)))
	{
		ft_strdel((char **)&buf);
		return (ER_MALLOC);
	}
	if (read(fd, buf, COMMENT_LENGTH) < 1)
	{
		ft_strdel((char **)&buf);
		return (ER_CHAMPION);
	}
	ft_memcpy(env->champ[env->nb - 1].head.comment, buf, COMMENT_LENGTH);
	env->champ[env->nb - 1].head.comment[COMMENT_LENGTH] = '\0';
	ft_strdel((char **)&buf);
	return (0);
}

int		set_prog_name(int fd, t_env *env)
{
	unsigned char	*buf;

	if (!(buf = (unsigned char *)ft_memalloc(sizeof(char)
		* PROG_NAME_LENGTH + 1)))
	{
		ft_strdel((char **)&buf);
		return (ER_MALLOC);
	}
	if (read(fd, buf, PROG_NAME_LENGTH) < 1)
	{
		ft_strdel((char **)&buf);
		return (ER_CHAMPION);
	}
	ft_memcpy(env->champ[env->nb - 1].head.prog_name, buf, PROG_NAME_LENGTH);
	env->champ[env->nb - 1].head.prog_name[PROG_NAME_LENGTH] = '\0';
	ft_strdel((char **)&buf);
	return (0);
}

int		set_prog_size(int fd, t_env *env)
{
	unsigned char	tmp[4];

	if (read(fd, tmp, sizeof(int)) < 1)
		return (ER_CHAMPION);
	env->champ[env->nb - 1].head.prog_size = tmp[0] << 24 | tmp[1] << 16
		| tmp[2] << 8 | tmp[3];
	return (0);
}

int		set_magic(int fd, t_env *env)
{
	unsigned char	tmp[4];

	if (read(fd, tmp, sizeof(int)) < 1)
		return (ER_EMPTY);
	env->champ[env->nb - 1].head.magic = tmp[0] << 24 | tmp[1] << 16
		| tmp[2] << 8 | tmp[3];
	return (0);
}

int		set_command(int fd, t_env *env)
{
	unsigned int	ret;

	ret = 0;
	if (!(env->champ[env->nb - 1].code = ft_memalloc(sizeof(char)
			* (env->champ[env->nb - 1].head.prog_size + 1))))
		return (ER_MALLOC);
	if ((ret = (unsigned int)read(fd, env->champ[env->nb - 1].code,
			env->champ[env->nb - 1].head.prog_size)) < 1)
		return (ER_CHAMPION);
	env->champ[env->nb - 1].code[env->champ[env->nb - 1].head.prog_size] = '\0';
	if (ret != env->champ[env->nb - 1].head.prog_size || read(fd, NULL, 2) != 0)
		return (ER_CHAMPION);
	return (0);
}

/*
**	static int	check_last(int fd, t_env *env)
**	{a verifier car quand on modifie le 0a final ca modifie tout le .cor
**	int				ret;
**	char			tmp[1];
**	ret = 0;
**	if ((ret = read(fd, tmp, sizeof(char))) != 0)
**		return (-1);
**	if (env->champ[env->nb - 1].code[env->champ[env->nb - 1].head.prog_size]
**		!= END_COR)
**		return (-1);
**	return (1);
**	}
*/
