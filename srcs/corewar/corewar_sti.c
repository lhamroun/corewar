/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_ldi.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:08:17 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 11:50:13 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static int
	store_index_reg(t_process *ps, t_env *env, char first)
{
	char	second;
	char	third;
	int		pos;

	second = env->arena.map[(ps->pc + 3) % MEM_SIZE];
	third = env->arena.map[(ps->pc + 4) % MEM_SIZE];
	if (second < 1 || second > REG_NUMBER || third < 1 || third > REG_NUMBER)
		return (-1);
	pos = (ps->reg[second - 1] + ps->reg[third - 1]) % IDX_MOD;
	pos < 0 ? pos = pos + MEM_SIZE : 1;
	env->arena.map[pos % MEM_SIZE] = ps->reg[first - 1] >> 24;
	env->arena.map[(pos + 1) % MEM_SIZE] = ps->reg[first - 1] >> 16;
	env->arena.map[(pos + 2) % MEM_SIZE] = ps->reg[first - 1] >> 8;
	env->arena.map[(pos + 3) % MEM_SIZE] = ps->reg[first - 1];
	if (env->visu == 1)
		if (set_visu_color(env, ps, pos) == -1)
			return (-1);
	return (1);
}

static void
	store_index_reg_addr(t_process *ps, t_env *env, short ocp, char first)
{
	int		second;
	int		third;
	int		pos;

	if (ocp == 100 || ocp == 116)
	{
		second = (short)((env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]));
		if (ocp == 116)
			second = calc_second_sti(env, ps, second);
		third = env->arena.map[(ps->pc + 5) % MEM_SIZE];
		if (third < 1 || third > REG_NUMBER)
			return ;
		third = ps->reg[third - 1];
	}
	else
	{
		second = env->arena.map[(ps->pc + 3) % MEM_SIZE];
		if (second < 1 || second > REG_NUMBER)
			return ;
		third = (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	}
	pos = ps->pc + ((second + third) % IDX_MOD);
	store_index_reg_addr_calc(ps, env, pos, first);
}

static int
	store_index_addr(t_process *ps, t_env *env, short ocp, char first)
{
	short	third;
	int		second;
	int		pos;

	if (ocp == 104)
	{
		second = (short)((env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]));
		third = (env->arena.map[(ps->pc + 5) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 6) % MEM_SIZE]);
	}
	else
	{
		second = (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
		second = calc_second_sti(env, ps, second);
		third = (env->arena.map[(ps->pc + 5) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 6) % MEM_SIZE]);
	}
	pos = ps->pc + ((second + third) % IDX_MOD);
	if (store_index_reg_addr_calc(ps, env, pos, first) == -1)
		return (-1);
	return (1);
}

static char
	store_index(t_process *ps, t_env *env, short ocp)
{
	char		first;

	ocp = env->arena.map[(ps->pc + 1) % MEM_SIZE];
	first = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	if (first < 1 || first > REG_NUMBER)
		return (-1);
	if (ocp == 84)
	{
		if (store_index_reg(ps, env, first) == -1)
			return (-1);
		return (3);
	}
	else if (ocp == 88 || ocp == 100 || ocp == 116)
	{
		store_index_reg_addr(ps, env, ocp, first);
		return (4);
	}
	else if (ocp == 104 || ocp == 120)
	{
		if (store_index_addr(ps, env, ocp, first) == -1)
			return (-1);
		return (5);
	}
	return (-1);
}

int
	corewar_sti(t_env *env, t_process *ps)
{
	char		x;

	x = -1;
	if (ps->cycle == -1)
		ps->cycle = 23;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		x = store_index(ps, env, 0);
		if (x == 3)
			ps->pc = (ps->pc + 5) % MEM_SIZE;
		else if (x == 4)
			ps->pc = (ps->pc + 6) % MEM_SIZE;
		else
			ps->pc = (ps->pc + 7) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
