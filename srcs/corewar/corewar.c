/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/20 12:23:10 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 11:09:28 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

t_process	*kill_process(t_env *env, t_process *ps)
{
	t_process	*tmp;
	t_process	*tmp2;

	tmp = env->arena.ps;
	tmp2 = ps->next;
	if (tmp != ps)
		while (tmp && tmp->next != ps)
			tmp = tmp->next;
	else
	{
		env->arena.ps = env->arena.ps->next;
		free(ps->reg);
		ps->reg = NULL;
		free(ps);
		ps = env->arena.ps->next;
		return (ps);
	}
	free(ps->reg);
	ps->reg = NULL;
	free(ps);
	ps = NULL;
	tmp->next = tmp2;
	ps = tmp2;
	env->arena.nb_ps--;
	return (ps);
}

void		arena_set_cycle(t_env *env)
{
	t_process	*ps;

	ps = env->arena.ps;
	if (env->arena.live > NBR_LIVE || env->arena.check >= MAX_CHECKS)
	{
		env->arena.ctd -= CYCLE_DELTA;
		env->arena.check = 0;
	}
	else if (env->arena.check < MAX_CHECKS)
		env->arena.check++;
	while (ps)
	{
		if (ps->live == 0)
		{
			ps = kill_process(env, ps);
			continue ;
		}
		if (ps)
			ps = ps->next;
	}
}

void		arena_set_live(t_env *env)
{
	unsigned int	i;
	t_process		*ps;

	i = 0;
	ps = env->arena.ps;
	while (ps)
	{
		ps->live = 0;
		ps = ps->next;
	}
	while (i < env->nb)
		env->champ[i++].live = 0;
	env->arena.live = 0;
}

int			arena_check_cycle(t_env *env)
{
	if (env->arena.cycle % env->arena.ctd == 0)
	{
		if (env->arena.live == 0)
			return (-1);
		arena_set_cycle(env);
		arena_set_live(env);
	}
	if (env->arena.ctd <= 0)
		return (-1);
	return (1);
}

void		corewar(t_env *env)
{
	t_process	*ps;

	while (1)
	{
		if (env->visu == 0 && env->arena.cycle == env->d)
		{
			print_arena(env);
			cleaner(env);
		}
		if (env->visu == 1)
			ft_visu(env);
		kill_visu_color(env);
		ps = env->arena.ps;
		while (ps)
		{
			exec_instruction(env, ps);
			ps = ps->next;
		}
		env->arena.cycle++;
		if (arena_check_cycle(env) == -1)
			break ;
	}
	env->ncu.rmap ? ft_strdel((char **)&env->ncu.rmap) : 1;
	env->ncu.cmap ? ft_strdel((char **)&env->ncu.cmap) : 1;
}
