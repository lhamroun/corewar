/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_zjmp.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:10:06 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/23 17:46:20 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static void	calc_zjmp(t_env *env, t_process *ps)
{
	short	x;
	int		pos;

	pos = 0;
	x = (env->arena.map[(ps->pc + 1) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 2) % MEM_SIZE]);
	if (ps->carry == 1)
	{
		pos = ps->pc + (int)x % IDX_MOD;
		if (pos < 0)
			ps->pc = MEM_SIZE + pos;
		else
			ps->pc = pos % MEM_SIZE;
	}
	else
		ps->pc = (ps->pc + 3) % MEM_SIZE;
}

int			corewar_zjmp(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 18;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		calc_zjmp(env, ps);
		ps->cycle = -1;
	}
	return (1);
}
