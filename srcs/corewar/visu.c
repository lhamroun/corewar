/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visu.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/17 18:12:31 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 03:21:27 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void	kill_visu_color(t_env *env)
{
	t_visu			*tmp;
	t_visu			*tmp2;

	tmp = env->v;
	while (tmp)
	{
		tmp2 = tmp->next;
		free(tmp);
		tmp = tmp2;
	}
	env->v = NULL;
}

void	visu(t_env *env)
{
	if ((int)env->arena.cycle > 0)
	{
		system("clear");
		print_arena(env);
		usleep(1000);
	}
}

int		set_visu_color(t_env *env, t_process *ps, short arg)
{
	unsigned int	i;
	t_visu			*v;

	i = 0;
	if (!(v = ft_memalloc(sizeof(t_visu))))
		return (-1);
	v->pc = arg % MEM_SIZE;
	v->index = ps->index;
	while (i < env->nb)
	{
		if (env->champ[i].index == ps->index)
			break ;
		++i;
	}
	if (i == 0)
		v->color = 1;
	else if (i == 1)
		v->color = 2;
	else if (i == 2)
		v->color = 3;
	else if (i == 3)
		v->color = 4;
	v->next = env->v;
	env->v = v;
	return (1);
}
