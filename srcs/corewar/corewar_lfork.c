/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_lfork.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 19:07:25 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 03:20:28 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

t_process	*set_new_lprocess(t_process *ps, short x)
{
	int			j;
	int			pos;
	t_process	*tmp;

	j = 0;
	pos = 0;
	if (!(tmp = ft_memalloc(sizeof(t_process))))
		return (NULL);
	if (!(tmp->reg = ft_memalloc(sizeof(unsigned int) * REG_NUMBER)))
		return (NULL);
	while (j < REG_NUMBER)
	{
		tmp->reg[j] = ps->reg[j];
		++j;
	}
	tmp->carry = ps->carry;
	tmp->index = ps->index;
	tmp->cycle = -1;
	tmp->live = ps->live;
	pos = ps->pc + (x % MEM_SIZE);
	tmp->pc = pos < 0 ? MEM_SIZE + pos : pos % MEM_SIZE;
	return (tmp);
}

int			create_lprocess(t_process *ps, t_env *env)
{
	short		x;
	t_process	*tmp;

	x = (env->arena.map[(ps->pc + 1) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 2) % MEM_SIZE]);
	if (!(tmp = set_new_lprocess(ps, x)))
		return (-1);
	tmp->next = env->arena.ps;
	env->arena.ps = tmp;
	env->arena.nb_ps++;
	return (1);
}

int			corewar_lfork(t_env *env, t_process *ps)
{
	if (ps->cycle == -1)
		ps->cycle = 998;
	else if (ps->cycle > 0)
		ps->cycle--;
	if (ps->cycle == 0)
	{
		if (create_lprocess(ps, env) == -1)
			return (-1);
		ps->pc = (ps->pc + 3) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
