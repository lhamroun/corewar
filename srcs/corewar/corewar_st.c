/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_st.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/14 17:09:22 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/27 06:18:01 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

static int		st_paste(t_env *env, t_process *ps, int arg1, short arg2)
{
	if (arg2 + ps->pc >= 0)
	{
		arg2 = ps->pc + arg2 % IDX_MOD;
		env->arena.map[arg2 % MEM_SIZE] = ps->reg[arg1 - 1] >> 24;
		env->arena.map[(1 + arg2) % MEM_SIZE] = ps->reg[arg1 - 1] >> 16;
		env->arena.map[(2 + arg2) % MEM_SIZE] = ps->reg[arg1 - 1] >> 8;
		env->arena.map[(3 + arg2) % MEM_SIZE] = ps->reg[arg1 - 1];
	}
	else
	{
		arg2 = (MEM_SIZE + arg2 % IDX_MOD) + ps->pc;
		env->arena.map[arg2 % MEM_SIZE] = ps->reg[arg1 - 1] >> 24;
		env->arena.map[(arg2 + 1) % MEM_SIZE] = ps->reg[arg1 - 1] >> 16;
		env->arena.map[(arg2 + 2) % MEM_SIZE] = ps->reg[arg1 - 1] >> 8;
		env->arena.map[(arg2 + 3) % MEM_SIZE] = ps->reg[arg1 - 1];
	}
	if (env->visu == 1)
		if (set_visu_color(env, ps, arg2) == -1)
			return (-1);
	return (1);
}

char			st_copy_paste(t_env *env, t_process *ps, unsigned char ocp)
{
	unsigned char	arg1;
	short			arg2;

	arg2 = 0;
	arg1 = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	if (arg1 == 0 || arg1 > REG_NUMBER)
		return (-1);
	if (ocp == 80)
	{
		arg2 = env->arena.map[(ps->pc + 3) % MEM_SIZE];
		if (arg2 == 0 || arg2 > REG_NUMBER)
			return (-1);
		ps->reg[arg2 - 1] = ps->reg[arg1 - 1];
		return (0);
	}
	else if (ocp == 112)
	{
		arg2 = (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 4) % MEM_SIZE]);
		if (st_paste(env, ps, arg1, arg2) == -1)
			return (-1);
		return (1);
	}
	return (-1);
}

int				corewar_st(t_env *env, t_process *ps)
{
	char			i;

	i = 0;
	if (ps->cycle == -1)
		ps->cycle = 3;
	else if (ps->cycle > 0)
		ps->cycle--;
	else if (ps->cycle == 0)
	{
		i = st_copy_paste(env, ps, env->arena.map[(ps->pc + 1) % MEM_SIZE]);
		if (i == 0)
			ps->pc = (ps->pc + 4) % MEM_SIZE;
		else
			ps->pc = (ps->pc + 5) % MEM_SIZE;
		ps->cycle = -1;
	}
	return (1);
}
