/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_init_visu2.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 13:42:25 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/01 01:39:40 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

unsigned int	ft_op_cl_color(t_env *env, unsigned int z, WINDOW *map_corew,
int o)
{
	if (o == 1)
	{
		if (env->arena.map[env->ncu.cmpt] != 0 && z == 0)
			z = env->ncu.cmpt + env->champ[env->ncu.ch].head.prog_size;
		if (env->ncu.cmpt < z)
			wattron(map_corew, COLOR_PAIR(env->ncu.ch + 1));
	}
	else
	{
		if (z > env->ncu.cmpt)
			wattroff(map_corew, COLOR_PAIR(env->ncu.ch + 1));
		if (z < env->ncu.cmpt && z != 0)
		{
			z = 0;
			env->ncu.ch++;
		}
	}
	return (z);
}

void			ft_init_map2(t_env *env, WINDOW *map_corew, int a, int c)
{
	if (env->arena.map[env->ncu.cmpt] >= 0 &&
	env->arena.map[env->ncu.cmpt] < 16)
		mvwprintw(map_corew, a, c + 3, "0%x", env->arena.map[env->ncu.cmpt]);
	else
		mvwprintw(map_corew, a, c + 3, "%x", env->arena.map[env->ncu.cmpt]);
}

int				ft_init_map(t_env *env, WINDOW *map_corew)
{
	int					a;
	int					c;
	unsigned int		z;

	a = 2;
	c = 1;
	z = 0;
	if (init_cmap(env) || init_rmap(env))
		return (-1);
	while (++a < 67)
	{
		while (c < 192)
		{
			z = ft_op_cl_color(env, z, map_corew, 1);
			ft_init_map2(env, map_corew, a, c);
			z = ft_op_cl_color(env, z, map_corew, 2);
			env->ncu.cmpt++;
			c += 3;
		}
		c = 1;
	}
	env->ncu.ch = 0;
	wrefresh(map_corew);
	return (0);
}
