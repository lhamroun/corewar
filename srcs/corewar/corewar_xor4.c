/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   corewar_xor4.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/31 15:33:27 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/03/01 09:20:38 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void
	calc_xor_92_180(t_env *env, t_process *ps, unsigned char c)
{
	int		a;
	int		b;

	a = 0;
	b = 0;
	(void)c;
	a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	b = (env->arena.map[(ps->pc + 6) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 7) % MEM_SIZE]);
	b = env->arena.map[(ps->pc + b) % MEM_SIZE];
	ps->reg[c - 1] = a ^ b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
}

void
	calc_xor_92(t_env *env, t_process *ps, unsigned char ocp, unsigned char c)
{
	int		a;
	int		b;

	a = 0;
	b = 0;
	if (ocp == 180)
		calc_xor_92_180(env, ps, c);
	else if (ocp == 228)
	{
		a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 3) % MEM_SIZE]);
		b = (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 24)
			+ (env->arena.map[(ps->pc + 5) % MEM_SIZE] << 16)
			+ (env->arena.map[(ps->pc + 6) % MEM_SIZE] << 8)
			+ (env->arena.map[(ps->pc + 7) % MEM_SIZE]);
		a = env->arena.map[(ps->pc + a) % MEM_SIZE];
		ps->reg[c - 1] = a ^ b;
		if (ps->reg[c - 1] == 0)
			ps->carry = 1;
		else
			ps->carry = 0;
	}
}

int
	calc_xor_82_100(t_env *env, t_process *ps, unsigned char c)
{
	int		a;
	int		b;

	a = 0;
	b = 0;
	a = env->arena.map[(ps->pc + 2) % MEM_SIZE];
	b = (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 6) % MEM_SIZE]);
	if (!a || a > REG_NUMBER)
		return (11);
	a = ps->reg[a - 1];
	ps->reg[c - 1] = a ^ b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (1);
}

int
	calc_xor_82_148(t_env *env, t_process *ps, unsigned char c)
{
	int		a;
	int		b;

	a = 0;
	b = 0;
	a = (env->arena.map[(ps->pc + 2) % MEM_SIZE] << 24)
		+ (env->arena.map[(ps->pc + 3) % MEM_SIZE] << 16)
		+ (env->arena.map[(ps->pc + 4) % MEM_SIZE] << 8)
		+ (env->arena.map[(ps->pc + 5) % MEM_SIZE]);
	b = env->arena.map[(ps->pc + 6) % MEM_SIZE];
	if (!b || b > REG_NUMBER)
		return (11);
	b = ps->reg[b - 1];
	ps->reg[c - 1] = a ^ b;
	if (ps->reg[c - 1] == 0)
		ps->carry = 1;
	else
		ps->carry = 0;
	return (1);
}

int
	calc_xor_82(t_env *env, t_process *ps, unsigned char ocp, unsigned char c)
{
	if (ocp == 148)
	{
		if (calc_xor_82_148(env, ps, c) == 11)
			return (11);
	}
	else if (ocp == 100)
	{
		if (calc_xor_82_100(env, ps, c) == 11)
			return (11);
	}
	return (1);
}
