/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_update_proc.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clboutry <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/27 15:20:40 by clboutry          #+#    #+#             */
/*   Updated: 2020/03/01 01:40:06 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"
#include <ncurses.h>

void		ft_upd_proc2(WINDOW *map_corew, t_env *env, int a, int c)
{
	t_process	*tmp;

	tmp = env->arena.ps;
	while (tmp)
	{
		if ((unsigned int)tmp->pc == env->ncu.cmpt)
		{
			wattron(map_corew, COLOR_PAIR(tmp->index + 4));
			if ((env->arena.map[env->ncu.cmpt] >= 0 &&
						env->arena.map[env->ncu.cmpt] < 16))
				mvwprintw(map_corew, a, c + 3, "0%x",
						env->ncu.rmap[env->ncu.cmpt]);
			else
				mvwprintw(map_corew, a, c + 3, "%x",
						env->ncu.rmap[env->ncu.cmpt]);
			wattroff(map_corew, COLOR_PAIR(tmp->index + 4));
			break ;
		}
		tmp = tmp->next;
	}
}

void		ft_update_process(WINDOW *map_corew, t_env *env)
{
	int			a;
	int			c;

	a = 2;
	c = 1;
	env->ncu.cmpt = 0;
	while (++a < 67)
	{
		while (c < 192)
		{
			ft_upd_proc2(map_corew, env, a, c);
			c += 3;
			env->ncu.cmpt++;
		}
		c = 1;
	}
}
