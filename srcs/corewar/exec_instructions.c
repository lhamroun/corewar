/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exec_instructions.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/01/28 18:08:57 by lyhamrou          #+#    #+#             */
/*   Updated: 2020/02/01 16:09:01 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "corewar.h"

void	exec_instruction(t_env *env, t_process *ps)
{
	static int	(*f[NB_INSTRUCTION])(t_env *, t_process *) = {
		&corewar_live,
		&corewar_load,
		&corewar_st,
		&corewar_add,
		&corewar_sub,
		&corewar_and,
		&corewar_or,
		&corewar_xor,
		&corewar_zjmp,
		&corewar_ldi,
		&corewar_sti,
		&corewar_fork,
		&corewar_lld,
		&corewar_lldi,
		&corewar_lfork,
		&corewar_aff,
	};

	if (env->arena.map[ps->pc] == 0 || env->arena.map[ps->pc] > 16)
		ps->pc = (ps->pc + 1) % MEM_SIZE;
	else
		f[env->arena.map[ps->pc] - 1](env, ps);
}
