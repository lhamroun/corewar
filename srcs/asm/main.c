/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akremer <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 00:08:42 by akremer           #+#    #+#             */
/*   Updated: 2020/02/19 02:33:48 by akremer          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static void		init_handle(t_asm *handle, char *av)
{
	t_header		header;

	ft_bzero(&header, sizeof(t_header));
	handle->dafuq = PROG_NAME_LENGTH;
	handle->dafuq2 = COMMENT_LENGTH;
	header.prog_size = 0;
	header.prog_name[0] = -1;
	header.comment[0] = -1;
	header.magic = COREWAR_EXEC_MAGIC;
	handle->header = header;
	handle->size_prog_size = ft_nbrlen(
			(unsigned long long)handle->header.prog_size, 0, 16);
	handle->size_magic = ft_nbrlen(
			(unsigned long long)handle->header.magic, 0, 16);
	handle->fd_read = 0;
	if (handle->size_magic / 2)
		handle->odd = 0;
	else
		handle->odd = 1;
	handle->bin = NULL;
	handle->fd_write = 0;
	handle->av = av;
	handle->op_tab = gopt();
}

static void		all_good(t_asm *handle)
{
	char		*name;
	char		*tmp;
	size_t		len;

	len = ft_strlen(handle->av) - 1;
	if (!(name = ft_strdup(handle->av)))
		error_malloc(handle);
	name[len - 1] = '\0';
	tmp = name;
	while (len && name[len] != '/')
		len--;
	name += len + 1;
	ft_printf("LADIES AND GENTLEMANS !\n");
	ft_printf("ARE YOU READY FOR OUR NEW CHALLENGER : %s !\n", name);
	ft_printf("WAITING FOR A BLOODY FIGHT AT HIM NEW PLACE ");
	ft_printf("I NAMED : %s\n", handle->bin);
	free(tmp);
}

int				main(int ac, char **av)
{
	t_asm		handle;

	(void)av;
	ft_bzero(&handle, sizeof(handle));
	if (ac != 2)
		error_ac();
	init_handle(&handle, av[1]);
	open_bar(&handle);
	handle.fd_read = open(av[1], O_RDONLY);
	if (handle.fd_read < 2)
		error_open(&handle);
	parsing(&handle);
	check_parsing(&handle);
	fill_handle(&handle);
	change_label(&handle);
	print_cor(&handle);
	all_good(&handle);
	free_all(&handle);
	return (0);
}
