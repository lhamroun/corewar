/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akremer <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/01 15:35:18 by akremer           #+#    #+#             */
/*   Updated: 2020/02/14 07:14:17 by akremer          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void		error_label(t_asm *handle, char *buf)
{
	ft_printf("Error label name here : ");
	ft_printf("%s\nPick a good name for this one", buf);
	ft_printf(" ,like Rouge seems to be a good name for a label.\n");
	free_all(handle);
	exit(0);
}

void		error_header(t_asm *handle, char *buf)
{
	ft_printf("Hmmm the header got a prob !\n");
	ft_printf("Like missing comment or name !\n");
	free_all(handle);
	ft_strdel(&buf);
	exit(0);
}
