/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akremer <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/18 21:45:39 by akremer           #+#    #+#             */
/*   Updated: 2020/02/18 21:59:16 by akremer          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static char				*parse_name_helper(t_asm *handle, char *buf, char *tmp)
{
	char	*gnl;
	char	*comment;

	if (!(comment = (char*)malloc(sizeof(char) * handle->dafuq * 2))
			&& ft_strdel(&tmp))
		error_malloc(handle);
	ft_bzero(comment, sizeof(comment));
	if (ft_strlen(buf) > PROG_NAME_LENGTH)
		if (ft_strdel(&comment))
			return (NULL);
	comment = ft_strcpy(comment, buf);
	if (ft_strchr(comment, 34))
		return (comment);
	while (get_next_line(handle->fd_read, &gnl) > 0)
	{
		if ((ft_strlen(comment) + ft_strlen(gnl)) > PROG_NAME_LENGTH)
			if (ft_strdel(&comment) && ft_strdel(&gnl))
				return (NULL);
		comment[ft_strlen(comment)] = '\n';
		comment = ft_strcat(comment, gnl);
		if (ft_strchr(gnl, 34) && ft_strdel(&gnl))
			break ;
		ft_strdel(&gnl);
	}
	return (comment);
}

static int				parse_name_helper2(t_asm *handle, char *name)
{
	int		len_name;
	int		i;

	len_name = ft_strclen(name, '\"');
	if (len_name > PROG_NAME_LENGTH)
		return (-1);
	if (len_name == 0)
		handle->header.prog_name[0] = 0;
	else
		ft_strncpy(handle->header.prog_name, name, len_name);
	i = len_name + 1;
	while (name[i] == '\t' || name[i] == ' ')
		i++;
	return (i);
}

int						parse_name(t_asm *handle, char *buf, char *tmp)
{
	int				i;
	char			*name;

	i = 0;
	while (buf[i] == '\t' || buf[i] == ' ')
		i++;
	if (ft_strncmp(NAME_CMD_STRING, buf + i, ft_strlen(NAME_CMD_STRING)) != 0)
		return (parse_comment(handle, buf, tmp));
	i += ft_strlen(NAME_CMD_STRING);
	while (buf[i] == '\t' || buf[i] == ' ')
		i++;
	if (buf[i] != '\"')
		return (1);
	buf += i + 1;
	if (!(name = parse_name_helper(handle, buf, tmp)))
		return (1);
	if (!(i = parse_name_helper2(handle, name))
			&& ft_strdel(&name))
		return (1);
	if ((name[i] == '\0' || name[i] == COMMENT_CHAR)
			&& ft_strdel(&name))
		return (0);
	ft_strdel(&name);
	return (1);
}
