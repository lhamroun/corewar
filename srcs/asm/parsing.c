/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akremer <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/11/26 23:46:17 by akremer           #+#    #+#             */
/*   Updated: 2020/03/01 11:08:19 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

static char		*parse_comment_helper(t_asm *handle, char *buf, char *tmp)
{
	char	*gnl;
	char	*comment;

	if (!(comment = (char*)malloc(sizeof(char) * handle->dafuq2 * 2))
			&& ft_strdel(&tmp))
		error_malloc(handle);
	ft_bzero(comment, sizeof(comment));
	if ((ft_strlen(buf) > COMMENT_LENGTH) && ft_strdel(&comment))
		return (NULL);
	comment = ft_strcpy(comment, buf);
	if (ft_strchr(comment, 34))
		return (comment);
	while (get_next_line(handle->fd_read, &gnl) > 0)
	{
		if ((ft_strlen(comment) + ft_strlen(gnl)) > COMMENT_LENGTH)
			if (ft_strdel(&comment) && ft_strdel(&gnl))
				return (NULL);
		comment[ft_strlen(comment) + 1] = '\0';
		comment[ft_strlen(comment)] = '\n';
		comment = ft_strcat(comment, gnl);
		if (ft_strchr(gnl, 34) && ft_strdel(&gnl))
			break ;
		ft_strdel(&gnl);
	}
	return (comment);
}

static int		parse_comment_helper2(t_asm *handle, char *comment)
{
	int		len_name;
	int		i;

	i = 0;
	len_name = 0;
	len_name = ft_strclen(comment, '\"');
	if (len_name > COMMENT_LENGTH)
		return (0);
	if (len_name == 0)
		handle->header.comment[0] = 0;
	else
		ft_strncpy(handle->header.comment, comment, len_name);
	i = len_name + 1;
	while (comment[i] == 't' || comment[i] == ' ' || comment[i] == '\n')
		i++;
	return (i);
}

int				parse_comment(t_asm *handle, char *buf, char *tmp)
{
	int			i;
	char		*comment;

	i = 0;
	while (buf[i] == '\t' || buf[i] == ' ')
		i++;
	if (ft_strncmp(COMMENT_CMD_STRING, buf + i,
				ft_strlen(COMMENT_CMD_STRING)) != 0)
		return (parse_name(handle, buf, tmp));
	i += ft_strlen(COMMENT_CMD_STRING);
	while (buf[i] == '\t' || buf[i] == ' ')
		i++;
	if (buf[i] != '\"')
		return (1);
	buf += i + 1;
	if (!(comment = parse_comment_helper(handle, buf, tmp)))
		return (1);
	if (!(i = parse_comment_helper2(handle, comment))
			&& ft_strdel(&comment))
		return (1);
	if ((comment[i] == '\0' || comment[i] == COMMENT_CHAR)
			&& ft_strdel(&comment))
		return (0);
	ft_strdel(&comment);
	return (1);
}

static void		parsing_helper(t_asm *handle, char *buf, char *tmp)
{
	if (handle->header.prog_name[0] == -1 && ft_strncmp(
				NAME_CMD_STRING, buf, ft_strlen(NAME_CMD_STRING)) == 0)
	{
		if (parse_name(handle, buf, tmp) && ft_strdel(&tmp))
			error_name(handle);
	}
	else if (handle->header.comment[0] == -1 && ft_strncmp(buf,
				COMMENT_CMD_STRING, ft_strlen(COMMENT_CMD_STRING)) == 0)
	{
		if (parse_comment(handle, buf, tmp) && ft_strdel(&tmp))
			error_comment(handle);
	}
	else if (handle->header.comment[0] != -1
			&& handle->header.prog_name[0] != -1)
	{
		if (parse_instruc(handle, buf, 0) && ft_strdel(&tmp))
			error_instruc(handle, buf);
	}
	else
		error_header(handle, tmp);
}

int				parsing(t_asm *handle)
{
	char		*buf;
	char		*tmp;

	while (get_next_line(handle->fd_read, &buf) > 0)
	{
		tmp = buf;
		handle->count_line++;
		if (check_blanc(buf) && ft_strdel(&buf))
			continue ;
		while (*buf == ' ' || *buf == '\t')
			buf++;
		parsing_helper(handle, buf, tmp);
		ft_strdel(&tmp);
	}
	return (1);
}
