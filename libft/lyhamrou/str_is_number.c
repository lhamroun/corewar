/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   str_is_number.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lyhamrou <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/12/01 23:47:49 by lyhamrou          #+#    #+#             */
/*   Updated: 2019/12/02 00:03:20 by lyhamrou         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		str_is_number(char *str)
{
	int		i;
	int		nb;

	i = 0;
	nb = 0;
	while (str[i] && ft_isblank(str[i]) == 1)
		++i;
	if (str[i] == '-' || str[i] == '+')
		++i;
	while (str[i])
	{
		if (ft_isdigit(str[i]) == 1)
			nb = 1;
		else
		{
			if (ft_isdigit(str[i - 1]) == 1 && ft_isblank(str[i]) == 1)
				break ;
			return (0);
		}
		++i;
	}
	while (str[i] && ft_isblank(str[i]) == 1)
		++i;
	return (!str[i] && nb == 1 ? 1 : 0);
}
