/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akremer <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 14:36:45 by akremer           #+#    #+#             */
/*   Updated: 2020/02/18 19:39:37 by akremer          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/get_next_line.h"

static int			free_all(char **scrap, char *buf)
{
	int		i;

	i = 0;
	while (i < 4865)
	{
		if (scrap[i])
			free(scrap[i]);
		i++;
	}
	if (buf)
		ft_strdel(&buf);
	return (1);
}

static void			full_line_helper(char **scrap, int fd, char *tmp)
{
	free(scrap[fd]);
	scrap[fd] = tmp;
}

static int			full_line(char **scrap, char **line, int fd, int ret)
{
	char	*tmp;
	size_t	i;

	i = 0;
	while (scrap[fd][i] != '\n' && scrap[fd][i] != '\0')
		i++;
	if (scrap[fd][i] == '\n')
	{
		if (!(*line = ft_strndup(scrap[fd], i)) && free_all(scrap, NULL))
			return (-1);
		if (!(tmp = ft_strdup(scrap[fd] + i + 1)) && free_all(scrap, NULL))
			return (-1);
		full_line_helper(scrap, fd, tmp);
		if (scrap[fd][0] == '\0')
			ft_strdel(&scrap[fd]);
	}
	else if (scrap[fd][i] == '\0')
	{
		if (ret == BUFF_SIZE)
			return (get_next_line(fd, line));
		if (!(*line = ft_strdup(scrap[fd])) && free_all(scrap, NULL))
			return (-1);
		ft_strdel(&scrap[fd]);
	}
	return (1);
}

static int			get_next_line_helper(char **scrap,
		char *buf, int fd, char *tmp)
{
	if (scrap[fd] == NULL)
		if (!(scrap[fd] = ft_strnew(1)) && free_all(scrap, buf))
			return (1);
	if (!(tmp = ft_strjoin(scrap[fd], buf)) && free_all(scrap, buf))
		return (1);
	full_line_helper(scrap, fd, tmp);
	return (0);
}

int					get_next_line(const int fd, char **line)
{
	static char	*scrap[4864];
	char		*buf;
	char		*tmp;
	int			ret;

	tmp = NULL;
	if (!(buf = ft_memalloc(BUFF_SIZE + 1)) && free_all(scrap, buf))
		return (-1);
	if (fd < 0 || fd > 4864 || line == NULL || BUFF_SIZE == 0)
		return (-1);
	while ((ret = read(fd, buf, BUFF_SIZE)) > 0)
	{
		buf[ret] = '\0';
		if (get_next_line_helper(scrap, buf, fd, tmp))
			return (-1);
		if (ft_strchr(buf, '\n'))
			break ;
	}
	free(buf);
	if (ret < 0)
		return (-1);
	if (ret == 0 && (scrap[fd] == NULL || scrap[fd][0] == '\0'))
		return (0);
	return (full_line(scrap, line, fd, ret));
}
